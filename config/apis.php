<?php

/**
 * API urls required to perform requests.
 */

return [
    'avatar' => 'https://eu.ui-avatars.com/api/',
    'currencies' => 'http://api.nbp.pl/api/'
];
