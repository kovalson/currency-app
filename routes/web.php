<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/lang/{locale}", "LocaleController@switchLocale");

Auth::routes();

Route::get("/", function () {
    return view("welcome");
});

Route::get("/home", "HomeController@index")->name("home");

Route::middleware("auth")->group(function () {
    Route::get("/settings", "HomeController@settings")->name("settings");
    Route::post("/password/change", "SettingsController@changePassword")->name("changePassword");
    Route::get("/account/delete", "SettingsController@deleteAccount")->name("deleteAccount");

    Route::post("/home", "HomeController@index")->name("currency");

    Route::get("/currency/save/{code}", "UserDefinedCurrencyController@store")->name("saveCurrency");
    Route::get("/currency/remove/{code}", "UserDefinedCurrencyController@destroy")->name("removeCurrency");

    Route::get("/logout", "Auth\LoginController@logout");
});
