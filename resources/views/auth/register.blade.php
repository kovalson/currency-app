@extends("layouts.app")

@section("content")
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card p-4 border-0 shadow">
                <div class="card-header bg-white border-bottom-0">
                    <h1>
                        <span class="icon mr-2">
                            <i class="fas fa-file-signature"></i>
                        </span>
                        <span>
                            {{ __("Register") }}
                        </span>
                    </h1>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route("register") }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __("Name") }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error("name") is-invalid @enderror" name="name" value="{{ old("name") }}" required autofocus>
                                @error("name")
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __("Surname") }}</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control @error("surname") is-invalid @enderror" name="surname" value="{{ old("surname") }}" required>

                                @error("surname")
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nickname" class="col-md-4 col-form-label text-md-right">{{ __("Nickname") }}</label>

                            <div class="col-md-6">
                                <input id="nickname" type="text" class="form-control @error("nickname") is-invalid @enderror" name="nickname" value="{{ old("nickname") }}" required autocomplete="username">

                                @error("nickname")
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __("Password") }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error("password") is-invalid @enderror" name="password" required autocomplete="new-password">

                                <span class="text-primary">
                                    {{ __("Password must be at least 8 characters long.") }}
                                </span>

                                @error("password")
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __("Confirm Password") }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __("Register") }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @include("partials.footer")
        </div>
    </div>
</div>
@endsection
