@extends("layouts.app")

@section("content")
<div class="container-fluid">
    <div class="row">
        <div class="col-12 p-0">

            @if (session("error"))
                <div class="container">
                    <div class="alert alert-error text-center" role="alert">
                        {{ session("error") }}
                    </div>
                </div>
            @endif

            @if (session("success"))
                <div class="container">
                    <div class="alert alert-success text-center" role="alert">
                        {{ session("success") }}
                    </div>
                </div>
            @endif

            <div class="jumbotron text-center m-0 bg-light d-flex flex-column justify-content-center">
                <span class="icon display-1 mb-4">
                    <i class="fas fa-chart-line fa-2x"></i>
                </span>
                <h1 class="display-4">{{ __("Exchange Rates") }}</h1>
                <p class="lead col-xl-4 col-lg-5 col-md-7 col-sm-12 mr-auto ml-auto">
                    {{ __("app_description") }}
                </p>
                @guest
                    <p class="lead col-xl-4 col-lg-5 col-md-7 col-sm-12 mr-auto ml-auto">
                        <a href="{{ route("login") }}">{{ __("Login") }}</a>
                        {{ __("or") }}
                        <a href="{{ route("register") }}">{{ __("Register") }}</a>
                    </p>
                @endguest
            </div>
            @include("partials.footer")
        </div>
    </div>
</div>
@endsection
