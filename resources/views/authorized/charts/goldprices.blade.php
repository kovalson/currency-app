<div class="card p-2 p-md-4 border-0 shadow">
    <div class="card-header bg-white border-bottom-0">
        <h1>
            <span class="icon mr-2">
                <i class="fas fa-coins fa-sm"></i>
            </span>
                <span>
                {{ __("Gold prices") }}
            </span>
        </h1>
    </div>

    <div class="card-body">
        <div class="chart">
            {!! $goldPricesChart->container() !!}
        </div>

        <div class="mt-4 text-dark text-center font-italic">
            {{ __("Some prices might not appear on the chart. It happens when API provider did not record the price that day or if it is unavailable.") }}
        </div>
    </div>
</div>
{!! $goldPricesChart->script() !!}
