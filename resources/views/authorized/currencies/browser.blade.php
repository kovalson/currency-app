<div class="card border-0 p-4 shadow">
    <p class="lead">
        <span class="icon mr-2">
            <i class="fas fa-euro-sign"></i>
        </span>
        <span>
            {{ __("Check exchange rate") }}
        </span>
    </p>
    <form action="{{ route("currency") }}" method="post">
        @csrf

        <div class="form-group">
            <label for="currency">{{ __("Currency code") }}</label>
            <input class="form-control"
                   type="text"
                   name="currency"
                   id="currency"
                   placeholder="{{ __("CHF, USD, ...") }}"
                   value="{{ Request::has("currency") ? Request::get("currency") : "" }}"
                   pattern="[A-Z]{3}"
                   maxlength="3"
                   required>
        </div>

        <div class="form-group">
            <label for="date">{{ __("For day") }}</label>
            <input class="form-control"
                   type="date"
                   name="date"
                   id="date"
                   value="{{ Request::has("date") ? Request::get("date") : date("Y-m-d") }}"
                   required>
        </div>

        <div class="form-group">
            <button class="btn btn-primary">
                <span class="icon mr-2">
                    <i class="fas fa-search"></i>
                </span>
                <span>
                    {{ __("Search") }}
                </span>
            </button>
        </div>
    </form>
    <div>
        @if (!$exchangeRate->isEmpty())
            <hr>

            <table class="table table-borderless mt-2">
                <tbody>
                    <tr>
                        <th>{{ __("Currency") }}</th>
                        <td>
                            {{ $exchangeRate->getCurrencyName() }}
                            <abbr title="{{ __("API provides polish names for currencies.") }}" data-toggle="tooltip">(?)</abbr>
                        </td>
                    </tr>
                    <tr>
                        <th>{{ __("Exchange rate") }}</th>
                        <td>{{ $exchangeRate->getRate() }}</td>
                    </tr>
                    <tr>
                        <th>{{ __("Rate valid at") }}</th>
                        <td>{{ $exchangeRate->getDate() }}</td>
                    </tr>
                </tbody>
            </table>
            @if ($definedCurrencies && array_filter($definedCurrencies, function ($currency) { return $currency->getCode() === Request::get("currency"); }))
                <p class="lead text-primary">
                    <span class="icon mr-2">
                        <i class="fas fa-check"></i>
                    </span>
                    <span>
                        {{ __("You already track this currency.") }}
                    </span>
                </p>
            @else
                <a class="btn btn-primary" href="{{ route("saveCurrency", Request::get("currency")) }}">
                    <span class="icon mr-2">
                        <i class="fas fa-bookmark"></i>
                    </span>
                    <span>
                        {{ __("Track this currency") }}
                    </span>
                </a>
            @endif
        @endif

        @if ($exchangeRate->getError())
            <div class="mt-2 text-danger">
                {{ $exchangeRate->getError() }}
            </div>
        @endif
    </div>
</div>
