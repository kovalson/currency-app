@if (!$definedCurrencies)
    <div class="justify-content-center align-items-center">
        <p class="text-center mt-4 text-dark">
            {{ __("You don't have any defined currencies.") }}
            <br>
            {{ __("To track a currency, find it using the search tool.") }}
        </p>
    </div>
@else
    <div class="card p-2 p-md-4 mt-4 border-0 shadow">
        <div class="card-header display-1 bg-white border-bottom-0">
            <h1>
                <span class="icon mr-2 d-none d-md-inline">
                    <i class="fas fa-bookmark fa-sm"></i>
                </span>
                    <span>
                    {{ __("Tracked currencies") }}
                </span>
            </h1>
        </div>

        <div class="pr-4 pl-4">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __("No.") }}</th>
                            <th class="d-none d-md-table-cell">{{ __("Currency name") }}</th>
                            <th>{{ __("Currency code") }}</th>
                            <th>
                                <abbr title="{{ __("Showing latest available exchange rate") }}" data-toggle="tooltip">
                                    {{ __("Exchange rate") }}
                                </abbr>
                            </th>
                            <th>{{ __("For day") }}</th>
                            <th>{{ __("Remove") }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $counter = 1; @endphp
                    @foreach ($definedCurrencies as $exchangeRate)
                        <tr>
                            <td>{{ $counter++ }}</td>
                            <td class="d-none d-md-table-cell">{{ $exchangeRate->getCurrencyName() }}</td>
                            <td>{{ $exchangeRate->getCode() }}</td>
                            <td>{{ $exchangeRate->getRate() }}</td>
                            <td>{{ $exchangeRate->getDate() }}</td>
                            <td>
                                <a class="btn btn-danger btn-sm" href="{{ route("removeCurrency", $exchangeRate->getCode()) }}">
                                        <span class="icon">
                                            <i class="fas fa-trash fa-sm"></i>
                                        </span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ __("No.") }}</th>
                            <th class="d-none d-md-table-cell">{{ __("Currency name") }}</th>
                            <th>{{ __("Currency code") }}</th>
                            <th>
                                <abbr title="{{ __("Showing latest available exchange rate") }}" data-toggle="tooltip">
                                    {{ __("Exchange rate") }}
                                </abbr>
                            </th>
                            <th>{{ __("For day") }}</th>
                            <th>{{ __("Remove") }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endif
