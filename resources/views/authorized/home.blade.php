@extends("layouts.app")

@section("content")
<div class="container">

    @if (session("error"))
        <div class="alert alert-danger border-0 shadow rounded">
            {{ session("error") }}
        </div>
    @endif

    @if (session("success"))
        <div class="alert alert-success mb-4 border-0 shadow rounded">
            {{ session("success") }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-7">
            @include("authorized.charts.goldprices")
        </div>
        <div class="col-md-5 mt-4 mt-md-0 mt-lg-0 mt-xl-0">
            @include("authorized.currencies.browser")
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include("authorized.currencies.defined")
        </div>
    </div>
    @include("partials.footer")
</div>
@endsection
