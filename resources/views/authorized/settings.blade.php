@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @if (session("error"))
                    <div class="alert alert-danger border-0 shadow rounded">
                        {{ session("error") }}
                    </div>
                @endif

                @if (session("success"))
                    <div class="alert alert-success mb-4 border-0 shadow rounded">
                        {{ session("success") }}
                    </div>
                @endif

                <div class="card p-2 p-md-4 border-0 shadow">
                    <div class="card-header bg-white border-bottom-0">
                        <h1>
                            <span class="icon mr-2 d-none d-md-inline">
                                <i class="fas fa-cog fa-sm"></i>
                            </span>
                            <span>
                                {{ __("Account settings") }}
                            </span>
                        </h1>
                    </div>

                    <div class="card-body">

                        <form action="{{ route('changePassword') }}" method="post">
                            @csrf

                            <p class="font-weight-bold lead mb-4 border-bottom">
                                {{ __("Change your password") }}
                            </p>

                            <div class="form-group row">
                                <label for="current_password" class="col-md-4 col-form-label text-md-right">{{ __('Current password') }}</label>

                                <div class="col-md-6">
                                    <input id="current_password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password" required>

                                    @error('current_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="new_password" class="col-md-4 col-form-label text-md-right">{{ __('New password') }}</label>

                                <div class="col-md-6">
                                    <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" required>

                                    <span class="text-primary">
                                        {{ __("Password must be at least 8 characters long.") }}
                                    </span>

                                    @error('new_password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="new_password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm new password') }}</label>

                                <div class="col-md-6">
                                    <input id="new_password_confirmation" type="password" class="form-control @error('new_password_confirmation') is-invalid @enderror" name="new_password_confirmation" required>

                                    @error('new_password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button class="btn btn-primary">
                                        <span class="icon mr-2">
                                            <i class="fas fa-check"></i>
                                        </span>
                                        <span>
                                            {{ __("Change password") }}
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <p class="font-weight-bold lead mt-4 border-bottom">
                            {{ __("Delete your account") }}
                        </p>

                        <a href="{{ route("deleteAccount") }}" class="btn btn-danger" onclick='{{ "return confirm('". __("Are you sure?") ."')" }}'>
                            <span class="icon mr-2">
                                <i class="fas fa-trash-alt"></i>
                            </span>
                            <span>
                                {{ __("Delete account") }}
                            </span>
                        </a>
                        <p class="text-danger mt-2">
                            {{ __("Warning! This action is irreversible!") }}
                        </p>
                    </div>
                </div>
                @include("partials.footer")
            </div>
        </div>
    </div>
@endsection
