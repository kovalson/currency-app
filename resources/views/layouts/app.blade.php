<!DOCTYPE html>
<html lang="{{ str_replace("_", "-", app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Krzysztof Tatarynowicz">
    <meta name="description" content="{{ __("app_description") }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __("Exchange Rates") }}</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="{{ asset("js/app.js") }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset("css/app.css") }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ Auth::check() ? url("home") : url("/") }}">
                    <span class="icon mr-2">
                        <i class="fas fa-home"></i>
                    </span>
                    <span>
                        {{ __("Exchange Rates") }}
                    </span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __("Toggle navigation") }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a id="languageNavbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="icon mr-2">
                                    <i class="fas fa-globe-europe"></i>
                                </span>
                                <span>
                                    {{ __("Language") }} ({{ session("applocale") ? session("applocale") : Config::get("app")["fallback_locale"] }})
                                </span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languageNavbarDropdown">
                                @foreach (Config::get("languages") as $short => $lang)
                                    <a href="{{ url("/lang/". $short) }}" class="dropdown-item {{ (session("applocale") && session("applocale") === $short) ? "active" : "" }}">{{ $lang }}</a>
                                @endforeach
                            </div>
                        </li>

                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link @if (Route::is("login")) active @endif" href="{{ route("login") }}">
                                    <span class="icon mr-2">
                                        <i class="fas fa-lock"></i>
                                    </span>
                                    <span>
                                        {{ __("Login") }}
                                    </span>
                                </a>
                            </li>
                            @if (Route::has("register"))
                                <li class="nav-item">
                                    <a class="nav-link @if (Route::is("register")) active @endif" href="{{ route("register") }}">
                                        <span class="icon mr-2">
                                            <i class="fas fa-file-signature"></i>
                                        </span>
                                        <span>
                                            {{ __("Register") }}
                                        </span>
                                    </a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2">
                                        <img src="{{ Storage::disk("avatars")->url(Auth::user()->avatar) }}" alt="{{ __("Avatar") }}" width="24" height="24" />
                                    </span>
                                    <span>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item @if (Route::is("settings")) active @endif" href="{{ route("settings") }}">
                                        <span class="icon mr-2">
                                            <i class="fas fa-cog"></i>
                                        </span>
                                        <span>
                                            {{ __("Account settings") }}
                                        </span>
                                    </a>
                                    <a class="dropdown-item" href="{{ route("logout") }}">
                                        <span class="icon mr-2">
                                            <i class="fas fa-sign-out-alt"></i>
                                        </span>
                                        <span>
                                            {{ __("Logout") }}
                                        </span>
                                    </a>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield("content")
        </main>
    </div>
</body>
</html>
