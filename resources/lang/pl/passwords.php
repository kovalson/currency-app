<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasła muszą być takie same i zawierać co najmniej 8 znaków.',
    'reset' => 'Hasło zostało zresetowane!',
    'sent' => 'Przesłaliśmy Ci link do resetowania hasła na skrzynkę e-mail!',
    'token' => 'Token do resetowania hasła jest nieprawidłowy.',
    'user' => "Nie znaleziono użytkownika z takim nickiem.",

];
