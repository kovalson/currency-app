<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Należy zaakceptować :attribute.',
    'active_url' => ':attribute nie jest prawidłowym URL.',
    'after' => ':attribute musi być datą po dacie :date.',
    'after_or_equal' => ':attribute musi być datą po dacie :date lub równą.',
    'alpha' => ':attribute może zawierać jedynie litery.',
    'alpha_dash' => ':attribute może zawierać jedynie litery, liczby, myślniki i podkreślniki.',
    'alpha_num' => ':attribute może zawierać jedynie litery i liczby.',
    'array' => ':attribute musi być tablicą.',
    'before' => ':attribute musi być datą przed datą :date.',
    'before_or_equal' => ':attribute musi być datą przed datą :date lub równą.',
    'between' => [
        'numeric' => ':attribute musi być między :min i :max.',
        'file' => ':attribute musi ważyć między :min i :max kilobajtów.',
        'string' => ':attribute musi mieć między :min i :max znaków.',
        'array' => ':attribute musi zawierać między :min i :max elementów.',
    ],
    'boolean' => ':attribute musi być prawdą lub fałszem.',
    'confirmed' => 'Potwierdzona wartość musi być taka sama.',
    'date' => ':attribute nie jest prawidłową datą.',
    'date_equals' => ':attribute musi być datą równą dacie :date.',
    'date_format' => ':attribute nie jest w formacie :format.',
    'different' => ':attribute i :other muszą się różnić.',
    'digits' => ':attribute musi mieć :digits cyfr.',
    'digits_between' => ':attribute musi mieć między :min i :max cyfr.',
    'dimensions' => ':attribute ma nieprawidłowe wymiary obrazka.',
    'distinct' => 'Pole :attribute zawiera duplikat.',
    'email' => ':attribute musi być prawidłowym adresem e-mail.',
    'ends_with' => ':attribute musi kończyć się na podane sposoby: :values',
    'exists' => 'Wybrany :attribute jest nieprawidłowy.',
    'file' => ':attribute musi być plikiem.',
    'filled' => ':attribute musi mieć wartość.',
    'gt' => [
        'numeric' => 'Parametr :attribute musi być większy niż :value.',
        'file' => ':attribute musi posiadać więcej niż :value kilobajtów.',
        'string' => ':attribute musi zawierać więcej niż :value znaków.',
        'array' => ':attribute musi zawierać więcej niż :value elementów.',
    ],
    'gte' => [
        'numeric' => 'Parametr :attribute musi być większy lub równy :value.',
        'file' => ':attribute musi posiadać więcej lub dokładnie :value kilobajtów.',
        'string' => ':attribute musi zawierać więcej lub dokładnie :value znaków.',
        'array' => ':attribute musi zawierać więcej lub dokładnie :value elementów.',
    ],
    'image' => ':attribute musi być obrazem.',
    'in' => 'Wybrany :attribute jest nieprawidłowy.',
    'in_array' => ':attribute nie istnieje w :other.',
    'integer' => ':attribute musi być liczbą.',
    'ip' => ':attribute musi być prawidłowym adresem IP.',
    'ipv4' => ':attribute musi być prawidłowym adresem IPv4.',
    'ipv6' => ':attribute musi być prawidłowym adresem IPv6.',
    'json' => ':attribute musi być w formacie JSON.',
    'lt' => [
        'numeric' => 'Wartość :attribute musi być mniejsza niż :value.',
        'file' => ':attribute musi mieć mniej niż :value kilobajtów.',
        'string' => ':attribute musi zawierać mniej niż :value znaków.',
        'array' => ':attribute musi zawierać mniej niż :value elementów.',
    ],
    'lte' => [
        'numeric' => 'Wartość :attribute musi być mniejsza lub równa :value.',
        'file' => ':attribute musi mieć mniej lub równo :value kilobajtów.',
        'string' => ':attribute musi zawierać mniej lub dokładnie :value znaków.',
        'array' => ':attribute musi zawierać mniej lub dokładnie :value elementów.',
    ],
    'max' => [
        'numeric' => 'Wartość :attribute nie może być większa niż :max.',
        'file' => ':attribute nie może mieć więcej niż :max kilobajtów.',
        'string' => ':attribute nie może zawierać więcej niż :max znaków.',
        'array' => ':attribute nie może zawierać więcej niż :max elementów.',
    ],
    'mimes' => ':attribute musi być plikiem typu: :values.',
    'mimetypes' => ':attribute musi być plikiem typu: :values.',
    'min' => [
        'numeric' => 'Wartość pola :attribute musi być co najmniej równa :min.',
        'file' => 'Pole :attribute musi mieć co najmniej :min kilobajtów.',
        'string' => 'Pole :attribute musi zawierać co najmniej :min znaków.',
        'array' => 'Pole :attribute musi zawierać co najmniej :min elementów.',
    ],
    'not_in' => 'Wartość :attribute jest nieprawidłowa.',
    'not_regex' => 'Format :attribute jest nieprawidłowy.',
    'numeric' => ':attribute musi być liczbą.',
    'present' => ':attribute musi istnieć.',
    'regex' => 'Format :attribute jest nieprawidłowy.',
    'required' => 'Pole :attribute jest wymagane.',
    'required_if' => 'Pole :attribute jest wymagane gdy :other jest :value.',
    'required_unless' => 'Pole :attribute jest wymagane, chyba że :other jest jedną z: :values.',
    'required_with' => 'Pole :attribute jest wymagane, gdy istnieją: :values.',
    'required_with_all' => 'Pole :attribute jest wymagane, gdy istnieją wszystkie :values.',
    'required_without' => 'Pole :attribute jest wymagane, gdy nie ma :values.',
    'required_without_all' => 'Pole :attribute jest wymagane, gdy żadne z :values nie istnieje.',
    'same' => ':attribute i :other muszą być takie same.',
    'size' => [
        'numeric' => ':attribute musi być rozmiaru :size.',
        'file' => ':attribute musi mieć :size kilobajtów.',
        'string' => ':attribute musi zawierać :size znaków.',
        'array' => ':attribute musi zawierać :size elementów.',
    ],
    'starts_with' => ':attribute musi zaczynać się jedną z następujących wartości: :values',
    'string' => ':attribute musi być ciągiem znaków.',
    'timezone' => ':attribute musi być prawidłową strefą czasową.',
    'unique' => ':attribute jest już zajęty.',
    'uploaded' => 'Nie udało się wrzucić :attribute.',
    'url' => 'Format :attribute jest nieprawidłowy.',
    'uuid' => ':attribute musi być prawidłową wartością UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
