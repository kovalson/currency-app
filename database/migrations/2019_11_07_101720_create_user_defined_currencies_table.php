<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDefinedCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("user_defined_currencies", function (Blueprint $table) {
            $table->unsignedBigInteger("user_id");
            $table->string("currency_code", 3)->comment("International 3-letter currency code.");
            $table->timestamps();

            $table->foreign("user_id")
                ->references("id")->on("users")
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->unique(["user_id", "currency_code"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("user_defined_currencies");
    }
}
