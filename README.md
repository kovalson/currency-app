# Currency App

Web application showing its users current exchange rates and gold prices.

## Main assumptions

The app is available in two languages - polish and english. The language can be freely changed anytime while using the app.

### Users

The following options are available for app users:

- registration,
- logging in,
- password change,
- account deletion,
- defining observed currencies.

### Avatar

Each user has their Avatar. It is generated using simple API from [UI Avatars](https://ui-avatars.com).

### Currencies and gold prices

Each authorized user:

- is presented a chart of gold prices from the last 10 days,
- is presented current exchange rates for defined currencies,
- can check exchange rate of any currency of any day.

Used [NBP API](http://api.nbp.pl/).

## Installation

To install the app clone current repository:
```
git clone https://gitlab.com/kovalson/currency-app.git
```
or download the repository as compressed file and unpack it somewhere on your desktop.

## Compilation & Run

### Prerequisites

The following packages are required to compile the app:
- [PHP (>= 7.1.3)](https://www.php.net/) unless stated otherwise in `composer.json`
- [PHP Composer](https://getcomposer.org/)
- [Node.js](https://nodejs.org/)
- [npm](https://www.npmjs.com/)

Also, MySQL server and database named `currency` are required for the app to work. Database configuration can be changed in the `.env` file.

### Compilation

To compile the app run the following command in the root directory:
```
composer install
```

Should any error occur, follow the instructions included in the error message.
Then run the following command in order to compile SASS and JS files:
```
npm run dev
```
for development compilation or:
```
npm run prod
```
for compilation in production mode. Finally, run the migration to create database tables:
```
php artisan migrate
```

### Run

After successful compilation run the following command to run the server:
```
php artisan serve
```
and open output url in the browser (by default that would be `http://127.0.0.1:8000`).

### Trivia

Changing `.env` file or modifying migration files while running the app might cause it not to work properly as Laravel caches some of the data. If changes made are not visible, try to run the following commands to clear caches:
```
php artisan cache:clear
php artisan route:cache
php artisan config:clear
php artisan view:clear
```
then try to restart the app (re-enter `php artisan serve` command) and hard refresh the page (`CTRL` + `F5`).

## Libraries used

The following libraries were used in this project:

- [Bootstrap 4](https://getbootstrap.com/) - CSS framework,
- [Laravel Charts](https://charts.erik.cat/) - renders charts based on different JS charts libraries,
- [Guzzle](https://github.com/guzzle/guzzle) - PHP HTTP client to perform API requests,
- [date-input-polyfill](https://www.npmjs.com/package/date-input-polyfill) - polyfill for `date` input type which is unsupported by some browsers.
