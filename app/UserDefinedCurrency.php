<?php

namespace CurrencyApp;

use Illuminate\Database\Eloquent\Model;

class UserDefinedCurrency extends Model
{
    public function user()
    {
        return $this->belongsTo('\CurrencyApp\User');
    }
}
