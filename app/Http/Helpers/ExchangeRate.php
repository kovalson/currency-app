<?php namespace App\Http\Helpers;

use Config;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Request;

class ExchangeRate
{
    public static function Empty()
    {
        return new ExchangeRate();
    }

    public static function fromCode(string $code)
    {
        return new ExchangeRate([
            "currency" => $code
        ]);
    }

    private $currency;
    private $currencyName;
    private $rate;
    private $error;
    private $date;

    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            $this->currency = $data["currency"];
            $this->date = isset($data["date"]) ? $data["date"] : null;
            $this->requestExchangeRate("A");
            $this->requestExchangeRate("B");
        } else if (Request::has("currency")) {
            $request = Request::all();
            $this->currency = $request["currency"];
            $this->date = $request["date"];
            $this->requestExchangeRate("A");
            $this->requestExchangeRate("B");
            if ($this->isEmpty()) {
                $this->error = __("Currency was not recognized or there is no data available!");
            }
        }
    }

    private function requestExchangeRate(string $table)
    {
        if ($this->currencyName && $this->rate) {
            return;
        }

        $url = Config::get("apis.currencies", "http://api.nbp.pl/api/");
        $url .= "exchangerates/rates/". $table ."/". $this->currency ."/". ($this->date ? ($this->date . "/") : "") ."?format=JSON";
        $client = new Client();

        try {
            $response = $client->get($url);
        } catch (Exception $exception) {
            return;
        }

        $data = json_decode($response->getBody());
        $this->currencyName = $data->currency;
        $this->rate = $data->rates[0]->mid;
        $this->date = $this->date ? $this->date : $data->rates[0]->effectiveDate;
    }

    public function getCode()
    {
        return $this->currency;
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function getCurrencyName()
    {
        return $this->currencyName;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getError()
    {
        return $this->error;
    }

    public function isEmpty(): bool {
        return !$this->currencyName;
    }
}
