<?php

namespace CurrencyApp\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LocaleController extends Controller
{
    public function switchLocale(string $locale) {
        if (array_key_exists($locale, Config::get("languages"))) {
            Session::put("applocale", $locale);
        }
        return Redirect::back();
    }
}
