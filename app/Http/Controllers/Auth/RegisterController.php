<?php

namespace CurrencyApp\Http\Controllers\Auth;

use Config;
use CurrencyApp\User;
use CurrencyApp\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = "/home";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("guest");
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            "name" => ["required", "string", "max:255"],
            "surname" => ["required", "string", "max:255"],
            "nickname" => ["required", "string", "max:255", "unique:users"],
            "password" => ["required", "string", "min:8", "confirmed"],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \CurrencyApp\User
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function create(array $data)
    {
        $client = new Client();

        $defaultUrl = "https://eu.ui-avatars.com/api/";
        $name = $data["name"] ."+". $data["surname"];
        $url = Config::get("apis.avatar", $defaultUrl) ."?name=". $name ."&rounded=true";
        $response = $client->request("GET", $url);

        if ($response->getStatusCode() !== 200) {
            return response(__("Error creating avatar!"))->isServerError();
        }

        $avatarFileName = Str::slug($data["nickname"], "-"). ".png";
        Storage::disk("avatars")->put($avatarFileName, $response->getBody()->getContents());

        return User::create([
            "name" => $data["name"],
            "surname" => $data["surname"],
            "nickname" => $data["nickname"],
            "password" => Hash::make($data["password"]),
            "avatar" => $avatarFileName
        ]);
    }
}
