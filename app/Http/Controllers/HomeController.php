<?php

namespace CurrencyApp\Http\Controllers;

use App\Http\Helpers\ExchangeRate;
use CurrencyApp\Charts\GoldPricesChart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Show the application dashboard.
     *
     * @param Request|null $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $chart = new GoldPricesChart();
        $exchangeRate = new ExchangeRate();
        $userDefiendCurrencies = $this->getUserDefinedCurrencies();

        return view("authorized.home", [
            "goldPricesChart" => $chart,
            "exchangeRate" => $exchangeRate,
            "definedCurrencies" => $userDefiendCurrencies
        ]);
    }

    public function settings()
    {
        return view("authorized.settings");
    }

    private function getUserDefinedCurrencies()
    {
        $currencies = Auth::user()->definedCurrencies()->get();

        if ($currencies->isEmpty()) {
            return [];
        }

        return array_map(function ($userDefinedCurrency) {
            return ExchangeRate::fromCode($userDefinedCurrency["currency_code"]);
        }, $currencies->toArray());
    }

}
