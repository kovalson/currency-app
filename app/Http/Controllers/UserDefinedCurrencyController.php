<?php

namespace CurrencyApp\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserDefinedCurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = $request->code;

        $validatedData = Validator::make(["code" => $code], ["code" => "required|regex:/^[A-Z]{3}$/"], [
            "code.required" => __("Given currency code is empty!"),
            "code.regex" => __("Given currency code format is invalid!")
        ]);

        if ($validatedData->errors()->isNotEmpty()) {
            return redirect()->back()->with("error", $validatedData->errors()->first());
        }

        $user = Auth::user();
        $definedCurrencies = $user->definedCurrencies();
        if ($definedCurrencies->where("currency_code", $code)->get()->isNotEmpty()) {
            return redirect()->back()->with("error", __("Currency is already being tracked!"));
        }

        $definedCurrencies->insert([
            "user_id" => $user->id,
            "currency_code" => $code
        ]);

        return redirect()
            ->back()
            ->with("success", __("Currency exchange rate is now being tracked!"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $code
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $code)
    {
        if (Auth::user()->definedCurrencies()->where("currency_code", $code)->delete()) {
            return redirect()->back()->with("success", __("Currency was successfully removed from the tracked list."));
        }
        return redirect()->back()->with("error", __("Given currency is not being tracked."));
    }
}
