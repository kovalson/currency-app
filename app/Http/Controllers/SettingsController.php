<?php

namespace CurrencyApp\Http\Controllers;

use Exception;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Validator;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();

        try {
            $this->validate($request, [
                "current_password" => ["required", "string", "min:8", function ($attribute, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->password)) {
                        return $fail(__("Current password is incorrect."));
                    }
                }],
                "new_password" => "required|string|different:current_password|min:8|confirmed"
            ], [
                "new_password.different" => __("New password cannot be the same as current.")
            ]);
        } catch (ValidationException $exception) {
            return redirect()->back()->withErrors($exception->errors());
        }

        $user->update([
            "password" => Hash::make($request->new_password)
        ]);

        return redirect()->back()->with("success", __("Successfully changed password!"));
    }

    public function deleteAccount()
    {
        $user = Auth::user();
        $avatar = $user->avatar;
        Auth::logout();

        try {
            $user->delete();
        } catch (Exception $exception) {
            return redirect("/")->with("error", $exception->getMessage());
        }

        Storage::disk("avatars")->delete($avatar);

        return redirect("/")->with("success", __("Your account has been successfully deleted."));
    }
}
