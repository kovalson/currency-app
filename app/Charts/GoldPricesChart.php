<?php

namespace CurrencyApp\Charts;

use Config;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use GuzzleHttp\Client;

class GoldPricesChart extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $today = date("Y-m-d");
        $tenDaysAgo = date("Y-m-d", strtotime("-10 days", strtotime($today)));

        $url = Config::get("apis.currencies", "http://api.nbp.pl/api/");

        // Disclaimer: there might be a better API endpoint to get 10 last gold prices,
        //  namely: http://api.nbp.pl/api/cenyzlota/last/10/?format=json, however,
        //  the task mentioned precisely "from ten last days" which may not result in 10 elements.
        $url .= "cenyzlota/". $tenDaysAgo ."/". $today ."/?format=JSON";

        // use Guzzle HTTP client
        $client = new Client();
        $response = $client->get($url);
        $goldPricesData = $response->getBody();

        $data = json_decode($goldPricesData);

        // prepare results for Chart js
        $dates = array_map(function ($element) {
            return $element->data;
        }, $data);

        $prices = array_map(function ($element) {
            return $element->cena;
        }, $data);

        $minPrice = min($prices);

        // set the chart
        $this->labels($dates);
        $this->dataset(__("Gold price"), "line", $prices)
            ->fill(false)
            ->color("#3490dc")
            ->lineTension(0);
        $this->displayLegend(false);
        $this->title(__("Gold prices within last 10 days"));
        $this->options([
            "scales" => [
                "yAxes" => [[
                    "display" => true,
                    "ticks" => [
                        "suggestedMin" => $minPrice,
                        "beginAtZero" => false
                    ]
                ]]
            ]
        ]);
    }
}
